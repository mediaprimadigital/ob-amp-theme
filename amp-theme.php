<?php
/*
Plugin Name: Ob Amp Theme
Plugin URI: https://wordpress.org/plugins/accelerated-mobile-pages/
Description: Create AMP Themes easily with help of AMP Framework
Version: 1.0
Author: Vishal Taj PM
Author URI: http://ampforwp.com/amp-theme-framework
License: GPL2
AMP: AMP Theme Framework 
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;